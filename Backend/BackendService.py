from Backend.AuthService import AuthService
from Backend.UserHealthService import UserHealthService
from typing import List

from utils.constants import *
from datetime import timedelta

class BackendService(object):
    def __init__(self):
        self.userHealthService = UserHealthService()
        self.authService = AuthService()

    def register(self, user:User) -> LoginStatus:
        return self.authService.register(user)

    def login(self, email:str, password) -> LoginStatus:
        # check database if password is correct
        return self.authService.login(email, password)

    def logout(self, user:User) -> LoginStatus:
        return self.authService.logout(user)

    def change_password(self, user: User, currentPassword:str, newPassword: str) -> LoginStatus:
        return self.authService.change_password(user, currentPassword, newPassword)

    def getDayActivity(self, user: User, date: datetime.date) -> HealthData:
        return self.userHealthService.getDayActivity(user, date)

    # Show Health Status Summary in UI

    def getWeeklyTrend(self, user:User) -> HealthData:
        healthDatas: List[HealthData] = self.getDateRangeActivity(u, datetime.date(
            datetime.today() - timedelta(days=6)), datetime.date(datetime.today()))

        #calculate the total of first half: HR, BP, Activity, Calin, Calout
        HR_left = 0
        BP_left = 0
        activity_left = 0
        calIn_left = 0
        calOut_left = 0
        for i in range(0, int(len(healthDatas) / 2)): #first half
            HR_left += healthDatas[i].heartRate
            BP_left += healthDatas[i].bloodPressure
            activity_left += healthDatas[i].activity
            calIn_left += healthDatas[i].calIn
            calOut_left += healthDatas[i].calOut

        # calculate the total of second half: HR, BP, Activity, Calin, Calout
        HR_right = 0
        BP_right = 0
        activity_right = 0
        calIn_right = 0
        calOut_right = 0
        for i in range(int(len(healthDatas) / 2), len(healthDatas)):  # second half
            HR_right += healthDatas[i].heartRate
            BP_right += healthDatas[i].bloodPressure
            activity_right += healthDatas[i].activity
            calIn_right += healthDatas[i].calIn
            calOut_right += healthDatas[i].calOut

        return HealthData(user.id, HR_right - HR_left, BP_right - BP_left,
                          activity_right - activity_left, calIn_right - calIn_left,
                          calOut_right - calOut_left )

    def getTodayActivity(self, user:User) -> HealthData:
        return self.userHealthService.getDayActivity(user, datetime.date(datetime.today()))

    #Show Graph in UI,
    def getDateRangeActivity(self, user:User, startDate: date, endDate: date) -> List[HealthData]:
        return self.userHealthService.getDateRangeActivity(user, startDate, endDate)

    # Data ENtry in UI, (add health data to DB)
    def addDataEntry(self, healthData:HealthData) -> bool:
        return self.userHealthService.addDataEntry(healthData)

if __name__ == '__main__':
    password = "123456"
    user = User('phuongngo1', 'phuongngo1@sjsu.edu', 'Phuong','Ngoc',
                'Ngo','male',  datetime.date(datetime.strptime('1111/1/1', '%Y/%m/%d')),
                float('12.3'), float('23.4'), password)

    backendService = BackendService()

    loginStatus:LoginStatus = backendService.register(user)

    loginStatus:LoginStatus = backendService.login("phuongngo1@sjsu.edu", password)
    print(loginStatus.status, loginStatus.message, loginStatus.user)
    u: User = loginStatus.user

    for day in range(0,8):
        health_data = HealthData(u.id, 125.2  + day, 86.7 + day, 12.4 + day*0.1, 1200.00 + day, 300.0 + day,
                                 datetime.date(datetime.today() - timedelta(days=day)))
        backendService.addDataEntry(health_data)


    healthData: HealthData = backendService.getDayActivity(u, datetime.date(datetime.today()))
    print(healthData)

    healthDatas: List[HealthData] = backendService.getDateRangeActivity(u, datetime.date(datetime.today() - timedelta(days=7)), datetime.date(datetime.today()))
    print(healthDatas)

    weeklyTrend:HealthData = backendService.getWeeklyTrend(u)
    print(weeklyTrend)

    loginStatus: LoginStatus = backendService.logout(loginStatus.user)

    # To change password
    # loginStatus: LoginStatus = backendService.change_password(loginStatus.user, "123456", "asdfghjkl")

    print("done")


