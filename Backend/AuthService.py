from utils.constants import *
from Backend.dbClient import *
import os

AUTH_DB = "auth.sqlite"

class AuthService:
    def __init__(self):
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "db", AUTH_DB)
        self.connection = create_connection(file_path)

        create_database_query = "CREATE TABLE IF NOT EXISTS user_data (\
                                   id integer NOT NULL,\
                                   email varchar(40) NOT NULL,\
                                   first_name varchar(15) NOT NULL,\
                                   middle_name varchar(15) NOT NULL,\
                                   last_name varchar(15) NOT NULL,\
                                   gender varchar(8) NOT NULL,\
                                   username varchar(40) NOT NULL,\
                                   password varchar(30) NOT NULL,\
                                   birthdate date,\
                                   height float,\
                                   weight float,\
                                   primary key(id) \
                               )"
        run_insert_query(self.connection, create_database_query)

    def register(self, user: User) -> LoginStatus:
        #database update to check if username exist
        #not exist -> register all properties in the user object
        # advance may need to check requirement for password > 6, uppercase...

        #Check username from database
        # run a query to read from database
        read_query = "SELECT * from user_data where username == '{}' or email == '{}'"\
            .format(user.username, user.email)
        existing_users = run_fetch_query(self.connection, read_query)
        # user already exits
        if len(existing_users):
            return LoginStatus(LoginReturnCode.USER_EXIST, "user or email already exits", user)

        if not user.password or not user.email:
            return LoginStatus(LoginReturnCode.UNKNOWN, "Missing username or password", user)

        create_user = "INSERT INTO user_data values({}, '{}', '{}','{}','{}','{}','{}','{}','{}', {}, {})" \
            .format(user.id, user.email, user.firstname, user.middlename, user.lastname,
                    user.gender, user.username, user.password,
                    user.dob, user.height, user.weight)

        id = run_insert_query(self.connection, create_user)

        user.password = ""
        user.id = id # need to give an id based on the number in database
        user.isLogin = True
        return LoginStatus(LoginReturnCode.OK, "ok", user)

    def login(self, email:str, password: str) -> LoginStatus:
        # check database if user and password is correct
        # Read query ->
        read_query = 'select * from user_data WHERE email == "{}" AND password == "{}"' \
            .format(email, password)
        existing_users = run_fetch_query(self.connection, read_query)

        # user not exits
        if not len(existing_users):
            return LoginStatus(LoginReturnCode.INVALID_PASSWORD, "invalid user", None)

        # pass login
        user: User = User(existing_users[0][6], existing_users[0][1], existing_users[0][2], existing_users[0][3],
                existing_users[0][4], existing_users[0][5], existing_users[0][8],
                existing_users[0][9], existing_users[0][10], existing_users[0][7], existing_users[0][0])
        user.password = ""
        user.isLogin = True

        return LoginStatus(LoginReturnCode.OK, "ok", user)

    def logout(self, user:User) -> LoginStatus:
        return LoginStatus(LoginReturnCode.OK, "ok", None)

    def change_password(self, user: User, currentPassword, newPassword ):
        # check database to see if current password is matched
        # return LoginStatus with the right status
        read_query = 'select * from user_data WHERE id == "{}" AND email == "{}" AND password == "{}"' \
            .format(user.id, user.email, currentPassword)
        existing_users = run_fetch_query(self.connection, read_query)

        # user not exits
        if not len(existing_users):
            return LoginStatus(LoginReturnCode.INVALID_PASSWORD, "invalid password", user)

        update_query = "update user_data set password = '{}' where id == '{}' and email = '{}' and password = '{}'"\
                        .format(newPassword, user.id, user.email, currentPassword)
        run_insert_query(self.connection, update_query)

        # user.password will hold the new password
        # after reset password, clear it so UI won't see
        user.password = ""
        return LoginStatus(LoginReturnCode.OK, "ok", user)



