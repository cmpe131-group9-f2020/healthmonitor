from utils.constants import *
from datetime import datetime, date
from typing import List
from Backend.dbClient import *
import os
AUTH_DB = "health.sqlite"

class UserHealthService:
    def __init__(self):
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "db", AUTH_DB)
        self.connection = create_connection(file_path)
        create_database_query = "CREATE TABLE IF NOT EXISTS health_data (\
                            user_id integer NOT NULL,\
                            heart_rate float,\
                            blood_pressure float,\
                            activity float,\
                            cal_in float,\
                            cal_out float,\
                            date date,\
                            primary key(user_id, date) \
                        )"
        run_insert_query(self.connection, create_database_query)

    def getDayActivity(self, user:User, d: date) -> HealthData:
        #take HR, BP, Activity, Calin,Calout
        #primary key: user_id and date
        read_query = 'select * from health_data WHERE user_id == "{}" AND date == "{}"' \
            .format(user.id, d)
        existing_data = run_fetch_query(self.connection, read_query)

        # pass login
        health_data: HealthData = HealthData(existing_data[0][0],existing_data[0][1]
                                             , existing_data[0][2],existing_data[0][3]
                                             , existing_data[0][4],existing_data[0][5],
                                             existing_data[0][6])
        return health_data

    def getDateRangeActivity(self, user:User, startDate: date, endDate: date) -> List[HealthData]:

        get_query = 'SELECT * from  health_data WHERE user_id = "{}" and "date"  between "{}" and "{}"' \
                    .format(user.id, startDate, endDate)
        healthRawDatas = run_fetch_query(self.connection, get_query)
        healthDatas: List[HealthData] = []
        for item in healthRawDatas:
            healthDatas.append(HealthData(item[0],item[1]
                                             , item[2],item[3]
                                             , item[4],item[5],
                                             item[6]))
        return healthDatas


    def addDataEntry(self, healthData:HealthData) -> bool:
        create_health = "INSERT INTO health_data values({}, '{}', '{}','{}','{}','{}','{}')" \
            .format (healthData.userId, healthData.heartRate, healthData.bloodPressure,
                     healthData.activity, healthData.calIn, healthData.calOut, healthData.date)
        run_insert_query(self.connection, create_health)
        return True