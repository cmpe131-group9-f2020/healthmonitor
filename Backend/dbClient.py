import sqlite3
from sqlite3 import Error

def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        # print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

def run_fetch_query(connection, query):
    # cursor = connection.cursor()
    try:
        ret = connection.execute(query)
        # print("ran query {} successfully".format(query))
        # connection.commit()
        return ret.fetchall()
    except Error as e:
        print(f"The error '{e}' occurred")

def run_insert_query(connection, query):
    # cursor = connection.cursor()
    try:
        ret = connection.execute(query)
        # print("ran query {} successfully".format(query))
        connection.commit()
        return ret.lastrowid
    except Error as e:
        print(f"The error '{e}' occurred")

if __name__ == '__main__':

    connection = create_connection("test.sqlite")
    # create_database_query = "CREATE TABLE IF NOT EXISTS projects (\
    # 	id integer PRIMARY KEY,\
    # 	name text NOT NULL,\
    # 	begin_date text,\
    # 	end_date text\
    # )"

    create_database_query = "CREATE TABLE IF NOT EXISTS user_data (\
        id integer NOT NULL,\
        first_name varchar(15) NOT NULL,\
        middle_name varchar(15) NOT NULL,\
        last_name varchar(15) NOT NULL,\
        gender varchar(8) NOT NULL,\
        username varchar(40) NOT NULL,\
        password varchar(30) NOT NULL,\
        birthdate date,\
        height float,\
        weight float,\
        primary key(id) \
    )"



    run_insert_query(connection, create_database_query)

    id = 'NULL'
    firstName = 'phuong'
    middleName = 'le'
    lastName = 'ngo'
    gender = 'male'
    username = 'phuong.ngo.le@sjsu.edu'
    password = 'iamphuong'
    birthdate = '1992/04/29'
    height = 5.06
    weight = 178.0

    create_user = "INSERT INTO user_data values({},'{}','{}','{}','{}','{}','{}','{}', {}, {})"\
        .format(id, firstName, middleName, lastName, gender, username, password, birthdate, height, weight )
    print(create_user)

    run_insert_query(connection, create_user)