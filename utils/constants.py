from typing import TypeVar, Generic
from logging import Logger
from datetime import datetime
from enum import Enum, auto
from datetime import datetime, date

class User():

    def __init__(self, username: str,
                 email: str,
                 firstname: str,
                 middlename: str,
                 lastname:str,
                 gender:str,
                 dob: date,
                 height: float,
                 weight: float,
                 password: str = "",
                 id: str = "NULL") -> None:
        self.username = username
        self.email = email
        self.firstname = firstname
        self.middlename = middlename
        self.lastname = lastname
        self.gender = gender
        self.dob = dob
        self.height = height
        self.weight = weight
        self.password = password
        self.id = id
        self.isLogin = False

    def __str__(self):
        return "User: id {}, email '{}', firstname '{}', middlename '{}'," \
               "lastname '{}',gender '{}',username '{}', password '{}',dob '{}', height {}, weight {}"\
            .format(self.id, self.email, self.firstname, self.middlename, self.lastname,
                self.gender, self.username, self.password,
                self.dob, self.height, self.weight)


class HealthData():
    def __init__(self, userId: str, heartRate: float, bloodPressure: float,
                 activity: float, calIn: float,
                 calOut: float, date: date=None) -> None:
        self.userId = userId
        self.heartRate = heartRate
        self.bloodPressure = bloodPressure
        self.activity = activity
        self.calIn = calIn
        self.calOut = calOut
        self.date = date

    def __str__(self):
        return "Health_data: user_id {}, heartRate '{}', bloodPressure '{}', activity '{}'," \
               "calIn '{}',calOut '{}',date '{}'"\
            .format(self.userId, self.heartRate, self.bloodPressure, self.activity, self.calIn,
                    self.calOut, self.date)



class LoginReturnCode(Enum):
    OK = auto(),
    USER_EXIST = auto(),
    INVALID_PASSWORD = auto(),
    UNSATISFY_PASSWORD = auto(), #for register/reset, when user create password too short?
    UNKNOWN = auto()



class LoginStatus():
    def __init__(self, status: LoginReturnCode, message: str, user: User) -> None:
        self.status = status
        self.message = message
        self.user = user

