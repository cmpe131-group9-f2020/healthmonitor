from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from UI.reset_pw_with_id import Ui_resetpw
# import mysql.connector

import sqlite3
from sqlite3 import Error

# mydb=mysql.connector.connect(
#     host="localhost",
#     user="root",
#     password="phuongngo",
#     database="health_monitor"
# )

def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


mydb = create_connection("test.sqlite")

class Ui_Data_Entry(object):
    def __init__(self,id):
        self.id =id
    def menu_selected(self):
        if self.comboBox.currentIndex() == 0 or self.comboBox.currentIndex() == 2:
            pass
        elif self.comboBox.currentIndex() == 1 or self.comboBox.currentIndex() == 5:
            pass
        elif self.comboBox.currentIndex() == 3:
            pass
        elif self.comboBox.currentIndex() == 4:
            self.resetpw = QtWidgets.QWidget()
            self.ui = Ui_resetpw(self.id)
            self.ui.setupUi(self.resetpw)
            self.resetpw.show()

    def clear_data(self):
        self.hr_lineedit.clear()
        self.bp_lineedit.clear()
        self.ac_hour_lineedit.clear()
        self.calin_lineedit.clear()
        self.calout_lineedit.clear()
    def confirm_clicked(self):
        if self.comboBox_2.currentIndex() == 0:
            query = "UPDATE monday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (int(self.hr_lineedit.text()),int(self.bp_lineedit.text()),int(self.ac_hour_lineedit.text()),
                                     int(self.calin_lineedit.text()),int(self.calout_lineedit.text()),self.id))
            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()

        elif self.comboBox_2.currentIndex() == 1:
            print("here")
            query = "UPDATE tuesday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
            int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
            int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))
            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()

        elif self.comboBox_2.currentIndex() == 2:
            query = "UPDATE wednesday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
                int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
                int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))

            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()
        elif self.comboBox_2.currentIndex() == 3:
            query = "UPDATE thursday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
            int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
            int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))

            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()
        elif self.comboBox_2.currentIndex() == 4:
            query = "UPDATE friday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
            int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
            int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))

            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()
        elif self.comboBox_2.currentIndex() == 5:
            query = "UPDATE saturday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
            int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
            int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))

            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()
        elif self.comboBox_2.currentIndex() == 6:
            query = "UPDATE sunday SET heart_rate=%s, blood_pressure=%s, activity_hr=%s, cal_in=%s, cal_out=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (
            int(self.hr_lineedit.text()), int(self.bp_lineedit.text()), int(self.ac_hour_lineedit.text()),
            int(self.calin_lineedit.text()), int(self.calout_lineedit.text()), self.id))

            mydb.commit()
            self.clear_data()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully update data.")
            message1.exec()
    def setupUi(self, Data_Entry):
        Data_Entry.setObjectName("Data_Entry")
        Data_Entry.resize(725, 777)
        Data_Entry.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QtWidgets.QLabel(Data_Entry)
        self.label.setGeometry(QtCore.QRect(0, 0, 731, 171))
        self.label.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.comboBox = QtWidgets.QComboBox(Data_Entry)
        self.comboBox.setGeometry(QtCore.QRect(480, 220, 231, 41))
        font = QtGui.QFont()
        font.setFamily("Palatino Linotype")
        font.setPointSize(14)
        self.comboBox.setFont(font)
        self.comboBox.setStyleSheet("background-color: rgb(232, 215, 255);")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        #self.comboBox.addItem("")
        self.heart_rate = QtWidgets.QLabel(Data_Entry)
        self.heart_rate.setGeometry(QtCore.QRect(10, 330, 241, 41))
        self.heart_rate.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.heart_rate.setObjectName("heart_rate")
        self.blood_pressure = QtWidgets.QLabel(Data_Entry)
        self.blood_pressure.setGeometry(QtCore.QRect(10, 400, 241, 41))
        self.blood_pressure.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.blood_pressure.setObjectName("blood_pressure")
        self.activity__hour = QtWidgets.QLabel(Data_Entry)
        self.activity__hour.setGeometry(QtCore.QRect(10, 470, 241, 41))
        self.activity__hour.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.activity__hour.setObjectName("activity__hour")
        self.Cal_IN = QtWidgets.QLabel(Data_Entry)
        self.Cal_IN.setGeometry(QtCore.QRect(10, 540, 241, 41))
        self.Cal_IN.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.Cal_IN.setObjectName("Cal_IN")
        self.Cal_OUT = QtWidgets.QLabel(Data_Entry)
        self.Cal_OUT.setGeometry(QtCore.QRect(10, 610, 241, 41))
        self.Cal_OUT.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.Cal_OUT.setObjectName("Cal_OUT")
        self.hr_lineedit = QtWidgets.QLineEdit(Data_Entry)
        self.hr_lineedit.setGeometry(QtCore.QRect(260, 330, 211, 41))
        self.hr_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.hr_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.hr_lineedit.setObjectName("hr_lineedit")
        self.bp_lineedit = QtWidgets.QLineEdit(Data_Entry)
        self.bp_lineedit.setGeometry(QtCore.QRect(260, 400, 211, 41))
        self.bp_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.bp_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.bp_lineedit.setObjectName("bp_lineedit")
        self.ac_hour_lineedit = QtWidgets.QLineEdit(Data_Entry)
        self.ac_hour_lineedit.setGeometry(QtCore.QRect(260, 470, 211, 41))
        self.ac_hour_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.ac_hour_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.ac_hour_lineedit.setObjectName("ac_hour_lineedit")
        self.calin_lineedit = QtWidgets.QLineEdit(Data_Entry)
        self.calin_lineedit.setGeometry(QtCore.QRect(260, 540, 211, 41))
        self.calin_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.calin_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.calin_lineedit.setObjectName("calin_lineedit")
        self.calout_lineedit = QtWidgets.QLineEdit(Data_Entry)
        self.calout_lineedit.setGeometry(QtCore.QRect(260, 610, 211, 41))
        self.calout_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.calout_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.calout_lineedit.setObjectName("calout_lineedit")
        self.confirm = QtWidgets.QPushButton(Data_Entry)
        self.confirm.setGeometry(QtCore.QRect(150, 690, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.confirm.setFont(font)
        self.confirm.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.confirm.setObjectName("confirm")
        self.exit = QtWidgets.QPushButton(Data_Entry)
        self.exit.setGeometry(QtCore.QRect(390, 690, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.exit.setFont(font)
        self.exit.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.exit.setObjectName("exit")
        self.comboBox_2 = QtWidgets.QComboBox(Data_Entry)
        self.comboBox_2.setGeometry(QtCore.QRect(60, 220, 151, 41))
        font = QtGui.QFont()
        font.setFamily("Palatino Linotype")
        font.setPointSize(14)
        self.comboBox_2.setFont(font)
        self.comboBox_2.setStyleSheet("background-color: rgb(232, 215, 255);")
        self.comboBox_2.setObjectName("comboBox_2")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")

        self.retranslateUi(Data_Entry)
        QtCore.QMetaObject.connectSlotsByName(Data_Entry)
        self.confirm.clicked.connect(self.confirm_clicked)
        self.comboBox.activated[str].connect(self.menu_selected)
        self.exit.clicked.connect(Data_Entry.close)
    def retranslateUi(self, Data_Entry):
        _translate = QtCore.QCoreApplication.translate
        Data_Entry.setWindowTitle(_translate("Data_Entry", "Data Entry"))
        self.comboBox.setItemText(0, _translate("Data_Entry", "Menu"))
        self.comboBox.setItemText(1, _translate("Data_Entry", "Health Status Summary"))
        self.comboBox.setItemText(2, _translate("Data_Entry", "Data Entry"))
        self.comboBox.setItemText(3, _translate("Data_Entry", "Show Graph"))
        self.comboBox.setItemText(4, _translate("Data_Entry", "Reset Password"))
        self.comboBox.setItemText(5, _translate("Data_Entry", "Exit"))
        self.heart_rate.setText(_translate("Data_Entry", "     Heart Rate"))
        self.blood_pressure.setText(_translate("Data_Entry", "Blood Pressure"))
        self.activity__hour.setText(_translate("Data_Entry", "   Activity Hour"))
        self.Cal_IN.setText(_translate("Data_Entry", "        Cal In"))
        self.Cal_OUT.setText(_translate("Data_Entry", "        Cal Out"))
        self.confirm.setText(_translate("Data_Entry", "Confirm"))
        self.exit.setText(_translate("Data_Entry", "exit"))
        self.comboBox_2.setItemText(0, _translate("Data_Entry", "Monday"))
        self.comboBox_2.setItemText(1, _translate("Data_Entry", "Tuesday"))
        self.comboBox_2.setItemText(2, _translate("Data_Entry", "Wednesday"))
        self.comboBox_2.setItemText(3, _translate("Data_Entry", "Thursday"))
        self.comboBox_2.setItemText(4, _translate("Data_Entry", "Friday"))
        self.comboBox_2.setItemText(5, _translate("Data_Entry", "Saturday"))
        self.comboBox_2.setItemText(6, _translate("Data_Entry", "Sunday"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Data_Entry = QtWidgets.QWidget()
    ui = Ui_Data_Entry()
    ui.setupUi(Data_Entry)
    Data_Entry.show()
    sys.exit(app.exec_())
