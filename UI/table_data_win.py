from PyQt5 import QtCore, QtGui, QtWidgets
from UI.reset_pw_with_id import Ui_resetpw
from UI.entry_data import Ui_Data_Entry

import sqlite3
from sqlite3 import Error

# mydb=mysql.connector.connect(
#     host="localhost",
#     user="root",
#     password="phuongngo",
#     database="health_monitor"
# )

def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

class Ui_data_table_2(object):
    def menu_selected(self):
        if self.comboBox.currentIndex()==0 or self.comboBox.currentIndex()==3:
            pass
        elif self.comboBox.currentIndex()==2:
            self.Data_Entry = QtWidgets.QWidget()
            self.ui = Ui_Data_Entry(self.id)
            self.ui.setupUi(self.Data_Entry)
            self.Data_Entry.show()
        elif self.comboBox.currentIndex()==4:
            self.resetpw = QtWidgets.QWidget()
            self.ui = Ui_resetpw(self.id)
            self.ui.setupUi(self.resetpw)
            self.resetpw.show()
    def __init__(self,id):
        self.id=id
    def display_data(self):
        mycursor = mydb.cursor()
        query2 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from monday where id =%s"
        query3 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from tuesday where id =%s"
        query4 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from wednesday where id =%s"
        query5 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from thursday where id =%s"
        query6 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from friday where id =%s"
        query7 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from saturday where id =%s"
        query8 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from sunday where id =%s"
        mycursor.execute(query2, (self.id,))
        result2 = mycursor.fetchall()
        mycursor.execute(query3, (self.id,))
        result3 = mycursor.fetchall()
        mycursor.execute(query4, (self.id,))
        result4 = mycursor.fetchall()
        mycursor.execute(query5, (self.id,))
        result5 = mycursor.fetchall()
        mycursor.execute(query6, (self.id,))
        result6 = mycursor.fetchall()
        mycursor.execute(query7, (self.id,))
        result7 = mycursor.fetchall()
        mycursor.execute(query8, (self.id,))
        result8 = mycursor.fetchall()
        result_set = []
        result_set.append(result2)
        result_set.append(result3)
        result_set.append(result4)
        result_set.append(result5)
        result_set.append(result6)
        result_set.append(result7)
        result_set.append(result8)

        index=0
        column=0
        row=0
        while index < 7:
            for x in result_set[index]:
                value = x
                index_of_while=0
                while index_of_while<5:
                    self.data_table.setItem(row,column,QtWidgets.QTableWidgetItem(str(value[index_of_while])))
                    row+=1
                    index_of_while+=1
                row=0
                column+=1
            index += 1
    def setupUi(self, data_table_2):
        data_table_2.setObjectName("data_table_2")
        data_table_2.resize(1112, 663)
        data_table_2.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QtWidgets.QLabel(data_table_2)
        self.label.setGeometry(QtCore.QRect(-90, 10, 1201, 201))
        self.label.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_table = QtWidgets.QLabel(data_table_2)
        self.label_table.setGeometry(QtCore.QRect(40, 230, 221, 41))
        self.label_table.setStyleSheet("font: 75 14pt \"Montserrat Subrayada\";\n"
"color: rgb(22, 11, 8);")
        self.label_table.setObjectName("label_table")
        self.data_table = QtWidgets.QTableWidget(data_table_2)
        self.data_table.setGeometry(QtCore.QRect(30, 320, 1051, 221))
        self.data_table.setStyleSheet("color: rgb(8, 8, 8);\n"
"font: 8pt \".VnClarendonH\";")
        self.data_table.setObjectName("data_table")
        self.data_table.setColumnCount(7)
        self.data_table.setRowCount(5)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setVerticalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        item.setBackground(QtGui.QColor(255, 255, 255))
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setForeground(brush)
        self.data_table.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setHorizontalHeaderItem(6, item)
        item = QtWidgets.QTableWidgetItem()
        brush = QtGui.QBrush(QtGui.QColor(6, 199, 6))
        brush.setStyle(QtCore.Qt.NoBrush)
        item.setBackground(brush)
        self.data_table.setItem(0, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(0, 6, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(1, 6, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(2, 6, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(3, 6, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 3, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 4, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 5, item)
        item = QtWidgets.QTableWidgetItem()
        self.data_table.setItem(4, 6, item)
        self.comboBox = QtWidgets.QComboBox(data_table_2)
        self.comboBox.setGeometry(QtCore.QRect(780, 240, 231, 41))
        font = QtGui.QFont()
        font.setFamily("Palatino Linotype")
        font.setPointSize(14)
        self.comboBox.setFont(font)
        self.comboBox.setStyleSheet("background-color: rgb(232, 215, 255);")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.copyright = QtWidgets.QLabel(data_table_2)
        self.copyright.setGeometry(QtCore.QRect(-30, 580, 1231, 61))
        self.copyright.setStyleSheet("font: 20pt \"Russo One\";\n"
"")
        self.copyright.setObjectName("copyright")
        self.display_data()
        self.retranslateUi(data_table_2)
        self.comboBox.activated[str].connect(self.menu_selected)
        QtCore.QMetaObject.connectSlotsByName(data_table_2)

    def retranslateUi(self, data_table_2):
        _translate = QtCore.QCoreApplication.translate
        data_table_2.setWindowTitle(_translate("data_table_2", "Show Data Table"))
        self.label_table.setText(_translate("data_table_2", "View Table Data"))
        item = self.data_table.verticalHeaderItem(0)
        item.setText(_translate("data_table_2", "Heart Rate"))
        item = self.data_table.verticalHeaderItem(1)
        item.setText(_translate("data_table_2", "Blood Pressure"))
        item = self.data_table.verticalHeaderItem(2)
        item.setText(_translate("data_table_2", "Activity Hour"))
        item = self.data_table.verticalHeaderItem(3)
        item.setText(_translate("data_table_2", "Cal In"))
        item = self.data_table.verticalHeaderItem(4)
        item.setText(_translate("data_table_2", "Cal Out"))
        item = self.data_table.horizontalHeaderItem(0)
        item.setText(_translate("data_table_2", "Monday"))
        item = self.data_table.horizontalHeaderItem(1)
        item.setText(_translate("data_table_2", "Tuesday"))
        item = self.data_table.horizontalHeaderItem(2)
        item.setText(_translate("data_table_2", "Wednesday"))
        item = self.data_table.horizontalHeaderItem(3)
        item.setText(_translate("data_table_2", "Thursday"))
        item = self.data_table.horizontalHeaderItem(4)
        item.setText(_translate("data_table_2", "Friday"))
        item = self.data_table.horizontalHeaderItem(5)
        item.setText(_translate("data_table_2", "Saturday"))
        item = self.data_table.horizontalHeaderItem(6)
        item.setText(_translate("data_table_2", "Sunday"))
        __sortingEnabled = self.data_table.isSortingEnabled()
        self.data_table.setSortingEnabled(False)
        self.data_table.setSortingEnabled(__sortingEnabled)
        self.comboBox.setItemText(0, _translate("data_table_2", "Menu"))
        self.comboBox.setItemText(1, _translate("data_table_2", "Health Status Summary"))
        self.comboBox.setItemText(2, _translate("data_table_2", "Data Entry"))
        self.comboBox.setItemText(3, _translate("data_table_2", "Show Table Data"))
        self.comboBox.setItemText(4, _translate("data_table_2", "Reset Password"))
        self.copyright.setText(_translate("data_table_2", "                                    CopyRight @ 2020 Group 9"))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    data_table_2 = QtWidgets.QWidget()
    ui = Ui_data_table_2(1)
    ui.setupUi(data_table_2)
    data_table_2.show()
    sys.exit(app.exec_())
