from lib.thread_local import ThreadLocalStore
from Backend.BackendService import BackendService

class UIObject(object):
    def __init__(self):
        self.backendService = getBackendService()

def getBackendService():
    return ThreadLocalStore().backendService

def createBackendService():
    ThreadLocalStore().backendService = BackendService()
    return ThreadLocalStore().backendService