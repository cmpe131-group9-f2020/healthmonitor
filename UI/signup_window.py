from PyQt5 import QtCore, QtGui, QtWidgets
# import mysql.connector
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QDoubleValidator
from UI.global_service import UIObject

from utils.constants import *
# mydb = mysql.connector.connect(
#     host="localhost",
#     user="root",
#     passwd="phuongngo",
#     database="health_monitor"
# )

class Ui_registerwindow(UIObject):

    def signup_clicked(self):
        # user = User(self.username_text.text(), self.email_text.text(), self.fname.text(),self.mname.text(),
        #             self.lname.text(), 'male', datetime.strptime(self.dateob.text(), '%Y/%m/%d'), float(self.w.text()), float(self.h.text()))

        user = User('username', 'email@email.com', 'firstname', 'middlename',
                    'lastname', 'male', datetime.date(datetime.strptime('1999/1/1', '%Y/%m/%d')), float('432.1'), float('123.4'), '123456789')

        loginStatus: LoginStatus = self.backendService.register(user)
        print(loginStatus.status, loginStatus.message)
        # mycursor = mydb.cursor()
        # query = "INSERT INTO user_data(fname,middle_name,lname,gender,username,passw,bdate,height,weight) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # if self.m.isChecked() ==True:
        #     self.sex ="male"
        # elif self.f.isChecked()==True:
        #     self.sex="female"
        # if self.middle.text()=="":
        #     self.useremail=f"{self.fname.text()}.{self.lname.text()}@sjsu.edu"
        #     mycursor.execute(query, (self.fname.text(), None, self.lname.text(), self.sex, self.useremail, self.passw.text(), self.dateob.text(),
        #     self.h.text(), self.w.text()))
        # elif self.middle.text()!="":
        #     self.useremail=f"{self.fname.text()}.{self.lname.text()}.{self.middle.text()}@sjsu.edu"
        #     mycursor.execute(query, (self.fname.text(), self.middle.text(), self.lname.text(), self.sex, self.useremail, self.passw.text(),
        #     self.dateob.text(), self.h.text(), self.w.text()))
        # mydb.commit()
        # message = QMessageBox()
        # message.setWindowTitle("Success")
        # message.setText("Successfully signed up.")
        # message.exec()
        # self.clear_clicked()

    def clear_clicked(self):
        self.fname.clear()
        self.lname.clear()
        self.middle.clear()
        self.passw.clear()
        self.dateob.clear()
        self.h.clear()
        self.w.clear()

    def setupUi(self, registerwindow):
        registerwindow.setObjectName("registerwindow")
        registerwindow.resize(812, 1102)
        registerwindow.setStyleSheet(
            "background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QtWidgets.QLabel(registerwindow)
        self.label.setGeometry(QtCore.QRect(10, 10, 731, 231))
        self.label.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.newuser = QtWidgets.QLabel(registerwindow)
        self.newuser.setGeometry(QtCore.QRect(0, 160, 741, 81))
        self.newuser.setStyleSheet("font: 18pt \"Russo One\";\n"
                                   "color: rgb(85, 5, 10)")
        self.newuser.setObjectName("newuser")
        self.f_name = QtWidgets.QLabel(registerwindow)
        self.f_name.setGeometry(QtCore.QRect(20, 320, 201, 41))
        self.f_name.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.f_name.setObjectName("f_name")
        self.l_name = QtWidgets.QLabel(registerwindow)
        self.l_name.setGeometry(QtCore.QRect(20, 370, 201, 41))
        self.l_name.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.l_name.setObjectName("l_name")
        self.mname = QtWidgets.QLabel(registerwindow)
        self.mname.setGeometry(QtCore.QRect(20, 420, 201, 41))
        self.mname.setStyleSheet("font: 18pt \"Russo One\";\n"
                                 "color: rgb(85, 5, 10)")
        self.mname.setObjectName("mname")
        self.gender = QtWidgets.QLabel(registerwindow)
        self.gender.setGeometry(QtCore.QRect(20, 620, 201, 41))
        self.gender.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.gender.setObjectName("gender")
        self.m = QtWidgets.QRadioButton(registerwindow)
        self.m.setGeometry(QtCore.QRect(240, 630, 111, 31))
        self.m.setStyleSheet("font: 18pt \"Russo One\";\n"
                             "color: rgb(85, 5, 10)")
        self.m.setObjectName("m")
        self.f = QtWidgets.QRadioButton(registerwindow)
        self.f.setGeometry(QtCore.QRect(380, 630, 141, 31))
        self.f.setStyleSheet("font: 18pt \"Russo One\";\n"
                             "color: rgb(85, 5, 10)")
        self.f.setObjectName("f")
        self.weight = QtWidgets.QLabel(registerwindow)
        self.weight.setGeometry(QtCore.QRect(20, 670, 201, 41))
        self.weight.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.weight.setObjectName("weight")
        self.height = QtWidgets.QLabel(registerwindow)
        self.height.setGeometry(QtCore.QRect(20, 720, 201, 41))
        self.height.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.height.setObjectName("height")
        self.date = QtWidgets.QLabel(registerwindow)
        self.date.setGeometry(QtCore.QRect(10, 570, 211, 41))
        self.date.setStyleSheet("font: 14pt \"Russo One\";\n"
                                "color: rgb(85, 5, 10)")
        self.date.setObjectName("date")
        self.signup = QtWidgets.QPushButton(registerwindow)
        self.signup.setGeometry(QtCore.QRect(90, 790, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.signup.setFont(font)
        self.signup.setStyleSheet("font: 18pt \"Russo One\";\n"
                                  "color: rgb(255, 247, 128);\n"
                                  "background-color: rgb(153, 0, 28);")
        self.signup.setObjectName("signup")
        self.clear = QtWidgets.QPushButton(registerwindow)
        self.clear.setGeometry(QtCore.QRect(290, 790, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.clear.setFont(font)
        self.clear.setStyleSheet("font: 18pt \"Russo One\";\n"
                                 "color: rgb(255, 247, 128);\n"
                                 "background-color: rgb(153, 0, 28);")
        self.clear.setObjectName("clear")
        self.exit = QtWidgets.QPushButton(registerwindow)
        self.exit.setGeometry(QtCore.QRect(490, 790, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.exit.setFont(font)
        self.exit.setStyleSheet("font: 18pt \"Russo One\";\n"
                                "color: rgb(255, 247, 128);\n"
                                "background-color: rgb(153, 0, 28);")
        self.exit.setObjectName("exit")
        self.pw = QtWidgets.QLabel(registerwindow)
        self.pw.setGeometry(QtCore.QRect(20, 520, 201, 41))
        self.pw.setStyleSheet("font: 18pt \"Russo One\";\n"
                              "color: rgb(85, 5, 10)")
        self.pw.setObjectName("pw")
        self.fname = QtWidgets.QLineEdit(registerwindow)
        self.fname.setGeometry(QtCore.QRect(230, 320, 391, 41))
        self.fname.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                 "font: 18pt \"Russo One\";\n"
                                 "color: rgb(85, 5, 10)")
        self.fname.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.fname.setObjectName("fname")
        self.lname = QtWidgets.QLineEdit(registerwindow)
        self.lname.setGeometry(QtCore.QRect(230, 370, 391, 41))
        self.lname.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                 "font: 18pt \"Russo One\";\n"
                                 "color: rgb(85, 5, 10)")
        self.lname.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.lname.setObjectName("lname")
        self.middle = QtWidgets.QLineEdit(registerwindow)
        self.middle.setGeometry(QtCore.QRect(230, 420, 391, 41))
        self.middle.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                  "font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.middle.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.middle.setObjectName("middle")
        self.passw = QtWidgets.QLineEdit(registerwindow)
        self.passw.setGeometry(QtCore.QRect(230, 520, 391, 41))
        self.passw.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                 "font: 18pt \"Russo One\";\n"
                                 "color: rgb(85, 5, 10)")
        self.passw.setEchoMode(QtWidgets.QLineEdit.PasswordEchoOnEdit)
        self.passw.setObjectName("passw")
        self.dateob = QtWidgets.QLineEdit(registerwindow)
        self.dateob.setGeometry(QtCore.QRect(230, 570, 391, 41))
        self.dateob.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                  "font: 18pt \"Russo One\";\n"
                                  "color: rgb(85, 5, 10)")
        self.dateob.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.dateob.setObjectName("dateob")
        self.w = QtWidgets.QLineEdit(registerwindow)
        self.w.setGeometry(QtCore.QRect(230, 670, 391, 41))

        self.onlyDouble = QDoubleValidator()
        self.w.setValidator(self.onlyDouble)
        self.w.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                             "font: 18pt \"Russo One\";\n"
                             "color: rgb(85, 5, 10)")
        self.w.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.w.setObjectName("w")
        self.h = QtWidgets.QLineEdit(registerwindow)
        self.h.setGeometry(QtCore.QRect(230, 720, 391, 41))

        self.onlyDouble = QDoubleValidator()
        self.h.setValidator(self.onlyDouble)
        self.h.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                             "font: 18pt \"Russo One\";\n"
                             "color: rgb(85, 5, 10)")
        self.h.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.h.setObjectName("h")
        self.username_label = QtWidgets.QLabel(registerwindow)
        self.username_label.setGeometry(QtCore.QRect(20, 270, 201, 41))
        self.username_label.setStyleSheet("font: 18pt \"Russo One\";\n"
                                          "color: rgb(85, 5, 10)")
        self.username_label.setObjectName("username_label")
        self.username_text = QtWidgets.QLineEdit(registerwindow)
        self.username_text.setGeometry(QtCore.QRect(230, 270, 391, 41))
        self.username_text.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                         "font: 18pt \"Russo One\";\n"
                                         "color: rgb(85, 5, 10)")
        self.username_text.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.username_text.setObjectName("username_text")
        self.email_label = QtWidgets.QLabel(registerwindow)
        self.email_label.setGeometry(QtCore.QRect(20, 470, 201, 41))
        self.email_label.setStyleSheet("font: 18pt \"Russo One\";\n"
                                       "color: rgb(85, 5, 10)")
        self.email_label.setObjectName("email_label")
        self.email_text = QtWidgets.QLineEdit(registerwindow)
        self.email_text.setGeometry(QtCore.QRect(230, 470, 391, 41))
        self.email_text.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                                      "font: 18pt \"Russo One\";\n"
                                      "color: rgb(85, 5, 10)")
        self.email_text.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email_text.setObjectName("email_text")

        self.retranslateUi(registerwindow)
        QtCore.QMetaObject.connectSlotsByName(registerwindow)

        # when buttons get clicked
        self.exit.clicked.connect(registerwindow.close)
        self.signup.clicked.connect(self.signup_clicked)
        self.clear.clicked.connect(self.clear_clicked)

    def retranslateUi(self, registerwindow):
            _translate = QtCore.QCoreApplication.translate
            registerwindow.setWindowTitle(_translate("registerwindow", "Register"))
            self.newuser.setText(_translate("registerwindow", "                                 New User"))
            self.f_name.setText(_translate("registerwindow", " First Name"))
            self.l_name.setText(_translate("registerwindow", " Last Name"))
            self.mname.setText(_translate("registerwindow", "Middle Name"))
            self.gender.setText(_translate("registerwindow", "     Gender"))
            self.m.setText(_translate("registerwindow", "Male"))
            self.f.setText(_translate("registerwindow", "Female"))
            self.weight.setText(_translate("registerwindow", "   Weight(lbs)"))
            self.height.setText(_translate("registerwindow", "     Height"))
            self.date.setText(_translate("registerwindow", "  DoB(yyyy/mm/dd)"))
            self.signup.setText(_translate("registerwindow", "Sign Up"))
            self.clear.setText(_translate("registerwindow", "Clear"))
            self.exit.setText(_translate("registerwindow", "Exit"))
            self.pw.setText(_translate("registerwindow", " Password"))
            self.passw.setText(_translate("registerwindow", ""))
            self.username_label.setText(_translate("registerwindow", " Username"))
            self.email_label.setText(_translate("registerwindow", " Email"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    registerwindow = QtWidgets.QWidget()
    ui = Ui_registerwindow()
    ui.setupUi(registerwindow)
    registerwindow.show()
    sys.exit(app.exec_())
