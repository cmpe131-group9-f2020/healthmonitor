from PyQt5 import QtCore, QtGui, QtWidgets
# import mysql.connector
from PyQt5.QtWidgets import QMessageBox

# mydb = mysql.connector.connect(
#     host="localhost",
#     user="root",
#     passwd="phuongngo",
#     database="health_monitor"
# )

class Ui_Form(object):
    def confirm_clicked(self):
        if self.pw_lineedit.text() != self.pw_lineedit_2.text():
            message = QMessageBox()
            message.setWindowTitle("FAIL")
            message.setText("Ooops! Password don't match.")
            message.exec()
            self.pw_lineedit.clear()
            self.pw_lineedit_2.clear()
        elif self.pw_lineedit.text() == self.pw_lineedit_2.text():
            query = "UPDATE user_data SET passw=%s where username=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (self.pw_lineedit.text(), self.email_lineedit.text()))
            mydb.commit()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully reset password.")
            message1.exec()
            self.pw_lineedit.clear()
            self.pw_lineedit_2.clear()
    def check_clicked(self):
        mycursor = mydb.cursor()
        query = "SELECT username FROM user_data WHERE username = %s"
        mycursor.execute(query, (self.email_lineedit.text(),))
        result = mycursor.fetchall()
        for x in result:
            emailid=x[0]
        if emailid == self.email_lineedit.text():
            message = QMessageBox()
            message.setWindowTitle("Success")
            message.setText("Valid User.")
            message.exec()
        self.confirm.clicked.connect(self.confirm_clicked)
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(875, 825)
        Form.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.resetpw = QtWidgets.QLabel(Form)
        self.resetpw.setGeometry(QtCore.QRect(10, 240, 861, 111))
        self.resetpw.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.resetpw.setObjectName("resetpw")
        self.image = QtWidgets.QLabel(Form)
        self.image.setGeometry(QtCore.QRect(-20, 10, 891, 251))
        self.image.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.image.setText("")
        self.image.setObjectName("image")
        self.email_userid = QtWidgets.QLabel(Form)
        self.email_userid.setGeometry(QtCore.QRect(10, 360, 251, 41))
        self.email_userid.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.email_userid.setObjectName("email_userid")
        self.email_lineedit = QtWidgets.QLineEdit(Form)
        self.email_lineedit.setGeometry(QtCore.QRect(270, 360, 391, 41))
        self.email_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.email_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email_lineedit.setObjectName("email_lineedit")
        self.check_but = QtWidgets.QPushButton(Form)
        self.check_but.setGeometry(QtCore.QRect(670, 360, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.check_but.setFont(font)
        self.check_but.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.check_but.setObjectName("check_but")
        self.newpw = QtWidgets.QLabel(Form)
        self.newpw.setGeometry(QtCore.QRect(20, 410, 241, 41))
        self.newpw.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.newpw.setObjectName("newpw")
        self.pw_lineedit = QtWidgets.QLineEdit(Form)
        self.pw_lineedit.setGeometry(QtCore.QRect(270, 410, 391, 41))
        self.pw_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.pw_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.pw_lineedit.setObjectName("pw_lineedit")
        self.newpw_2 = QtWidgets.QLabel(Form)
        self.newpw_2.setGeometry(QtCore.QRect(20, 460, 241, 41))
        self.newpw_2.setStyleSheet("font: 15pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.newpw_2.setObjectName("newpw_2")
        self.pw_lineedit_2 = QtWidgets.QLineEdit(Form)
        self.pw_lineedit_2.setGeometry(QtCore.QRect(270, 460, 391, 41))
        self.pw_lineedit_2.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.pw_lineedit_2.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.pw_lineedit_2.setObjectName("pw_lineedit_2")
        self.confirm = QtWidgets.QPushButton(Form)
        self.confirm.setGeometry(QtCore.QRect(250, 570, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.confirm.setFont(font)
        self.confirm.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.confirm.setObjectName("confirm")
        self.exit = QtWidgets.QPushButton(Form)
        self.exit.setGeometry(QtCore.QRect(480, 570, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.exit.setFont(font)
        self.exit.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.exit.setObjectName("exit")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        #when buttons get clicked
        self.check_but.clicked.connect(self.check_clicked)
        self.exit.clicked.connect(Form.close)
    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Reset a password"))
        self.resetpw.setText(_translate("Form", "                               Change a password"))
        self.email_userid.setText(_translate("Form", "           Email"))
        self.check_but.setText(_translate("Form", "Check"))
        self.newpw.setText(_translate("Form", " New Password"))
        self.newpw_2.setText(_translate("Form", "Confirm Password"))
        self.confirm.setText(_translate("Form", "Confirm"))
        self.exit.setText(_translate("Form", "exit"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
