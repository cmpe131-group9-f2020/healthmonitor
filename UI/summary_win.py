from PyQt5 import QtCore, QtGui, QtWidgets
from UI.reset_pw_with_id import Ui_resetpw
from UI.entry_data import Ui_Data_Entry
from UI.table_data_win import Ui_data_table_2
# import mysql.connector
import source
import sqlite3
from sqlite3 import Error

def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


mydb = create_connection("test.sqlite")

# mydb=mysql.connector.connect(
#     host="localhost",
#     user="root",
#     password="phuongngo",
#     database="health_monitor"
# )


class Ui_Summary(object):

    def __init__(self):
        pass

    def __init__(self,id):
        self.id=id
        self.display_userinfo()
        self.display_today_info()
        self.display_weekly_info()
    def combobox_selected(self):
        if self.comboBox.currentIndex() == 0 or self.comboBox.currentIndex() == 1:
            pass
        elif self.comboBox.currentIndex() == 2:
            self.Data_Entry = QtWidgets.QWidget()
            self.ui = Ui_Data_Entry(self.id)
            self.ui.setupUi(self.Data_Entry)
            self.Data_Entry.show()
        elif self.comboBox.currentIndex() == 3:
            self.data_table_2 = QtWidgets.QWidget()
            self.ui = Ui_data_table_2(self.id)
            self.ui.setupUi(self.data_table_2)
            self.data_table_2.show()
        elif self.comboBox.currentIndex() == 4:
            self.resetpw = QtWidgets.QWidget()
            self.ui = Ui_resetpw(self.id)
            self.ui.setupUi(self.resetpw)
            self.resetpw.show()
        elif self.comboBox.currentIndex() == 5:
            pass

    def display_weekly_info(self):
        self.sum_hr=0
        self.sum_bp=0
        self.sum_ac_hr=0
        self.sum_calin=0
        self.sum_calout=0
        # mycursor = mydb.cursor()
        # query2 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from monday where id =%s"
        # query3 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from tuesday where id =%s"
        # query4 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from wednesday where id =%s"
        # query5 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from thursday where id =%s"
        # query6 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from friday where id =%s"
        # query7 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from saturday where id =%s"
        # query8 = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from sunday where id =%s"
        # mycursor.execute(query2, (self.id,))
        # result2 = mycursor.fetchall()
        # mycursor.execute(query3, (self.id,))
        # result3 = mycursor.fetchall()
        # mycursor.execute(query4, (self.id,))
        # result4 = mycursor.fetchall()
        # mycursor.execute(query5, (self.id,))
        # result5 = mycursor.fetchall()
        # mycursor.execute(query6, (self.id,))
        # result6 = mycursor.fetchall()
        # mycursor.execute(query7, (self.id,))
        # result7 = mycursor.fetchall()
        # mycursor.execute(query8, (self.id,))
        # result8 = mycursor.fetchall()
        #
        # result_set = []
        # result_set.append(result2)
        # result_set.append(result3)
        # result_set.append(result4)
        # result_set.append(result5)
        # result_set.append(result6)
        # result_set.append(result7)
        # result_set.append(result8)
        #
        # index=0
        # while index < 7:
        #     for x in result_set[index]:
        #         self.sum_hr += x[0]
        #         self.sum_bp += x[1]
        #         self.sum_ac_hr += x[2]
        #         self.sum_calin += x[3]
        #         self.sum_calout += x[4]
        #     index += 1
        # self.sum_hr //= 7
        # self.sum_bp //= 7
        # self.sum_ac_hr //= 7
        # self.sum_calin //= 7
        # self.sum_calout //= 7

    def display_today_info(self):
        mycursor = mydb.cursor()
        query = "Select heart_rate,blood_pressure,activity_hr,cal_in,cal_out from monday where id =%s"
        # mycursor.execute(query, (self.id,))
        # result = mycursor.fetchall()
        result  = [123, 88, 10, 300, 200]
        # for x in result:
        self.heart_rate = result[0]
        self.b_press = result[1]
        self.ac_hour = result[2]
        self.calin_daily = result[3]
        self.calout_daily = result[4]

        self.heart_r = str(self.heart_rate)
        self.blood_p=str(self.b_press)
        self.ac_hr=f"{self.ac_hour}hr"
        self.calo_in=str(self.calin_daily)
        self.calo_out = str(self.calout_daily)
    def display_userinfo(self):
        mycursor = mydb.cursor()
        query = "Select fname,lname,gender,weight,height from user_data where id =%s"
        # mycursor.execute(query, (self.id,))
        # result = mycursor.fetchall()
        result = ['f', 'l', 's', 123.45, 5.6]
        # for x in result:
        self.fname = result[0]
        self.lname = result[1]
        self.sex = result[2]
        weight = result[3]
        height = result[4]
        kg = weight * 0.454
        self.weight = f"{weight}lbs({kg}kgs)"
        self.height = str(height)
        convert_height_to_in = height * 12
        bmi = (703 * weight) // ((convert_height_to_in) ** 2)
        self.bmi_calc = str(bmi)

    def setupUi(self, Summary):
        Summary.setObjectName("Summary")
        Summary.resize(1046, 852)
        font = QtGui.QFont()
        font.setFamily("VNI-Souvir")
        font.setBold(True)
        font.setWeight(75)
        Summary.setFont(font)
        Summary.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.image = QtWidgets.QLabel(Summary)
        self.image.setGeometry(QtCore.QRect(-10, 0, 1071, 181))
        self.image.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.image.setText("")
        self.image.setObjectName("image")
        self.user_info = QtWidgets.QGroupBox(Summary)
        self.user_info.setGeometry(QtCore.QRect(10, 240, 371, 441))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(50)
        self.user_info.setFont(font)
        self.user_info.setStyleSheet("font: 14pt \"MS Sans Serif\";\n"
"color: rgb(106, 0, 0);\n"
"background-color: rgb(219, 255, 240);")
        self.user_info.setFlat(False)
        self.user_info.setCheckable(False)
        self.user_info.setObjectName("user_info")
        self.f_name = QtWidgets.QLabel(self.user_info)
        self.f_name.setGeometry(QtCore.QRect(0, 80, 121, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.f_name.setFont(font)
        self.f_name.setObjectName("f_name")
        self.l_name = QtWidgets.QLabel(self.user_info)
        self.l_name.setGeometry(QtCore.QRect(0, 140, 121, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.l_name.setFont(font)
        self.l_name.setObjectName("l_name")
        self.gender = QtWidgets.QLabel(self.user_info)
        self.gender.setGeometry(QtCore.QRect(10, 200, 91, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.gender.setFont(font)
        self.gender.setObjectName("gender")
        self.wei = QtWidgets.QLabel(self.user_info)
        self.wei.setGeometry(QtCore.QRect(10, 260, 91, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.wei.setFont(font)
        self.wei.setObjectName("wei")
        self.hei = QtWidgets.QLabel(self.user_info)
        self.hei.setGeometry(QtCore.QRect(10, 320, 81, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.hei.setFont(font)
        self.hei.setObjectName("hei")
        self.bmi = QtWidgets.QLabel(self.user_info)
        self.bmi.setGeometry(QtCore.QRect(10, 380, 51, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.bmi.setFont(font)
        self.bmi.setObjectName("bmi")
        self.label = QtWidgets.QLabel(self.user_info)
        self.label.setGeometry(QtCore.QRect(60, 10, 231, 31))
        self.label.setStyleSheet("color: rgb(85, 170, 0);\n"
"text-decoration: underline;\n"
"font: 18pt \"Impact\";")
        self.label.setObjectName("label")
        self.fname_info = QtWidgets.QLabel(self.user_info)
        self.fname_info.setGeometry(QtCore.QRect(150, 80, 131, 31))
        self.fname_info.setText(self.fname.upper())
        self.fname_info.setObjectName("fname_info")
        self.lname_info = QtWidgets.QLabel(self.user_info)
        self.lname_info.setGeometry(QtCore.QRect(140, 140, 131, 31))
        self.lname_info.setText(self.lname.upper())
        self.lname_info.setObjectName("lname_info")
        self.weight_info = QtWidgets.QLabel(self.user_info)
        self.weight_info.setGeometry(QtCore.QRect(100, 260, 261, 31))
        self.weight_info.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.weight_info.setText(self.weight)
        self.weight_info.setObjectName("weight_info")
        self.height_info = QtWidgets.QLabel(self.user_info)
        self.height_info.setGeometry(QtCore.QRect(130, 320, 131, 31))
        self.height_info.setText(self.height)
        self.height_info.setObjectName("height_info")
        self.bmi_info = QtWidgets.QLabel(self.user_info)
        self.bmi_info.setGeometry(QtCore.QRect(70, 380, 291, 31))
        self.bmi_info.setText(self.bmi_calc)
        self.bmi_info.setObjectName("bmi_info")
        self.gender_info = QtWidgets.QLabel(self.user_info)
        self.gender_info.setGeometry(QtCore.QRect(140, 200, 131, 31))
        self.gender_info.setText(self.sex.upper())
        self.gender_info.setObjectName("gender_info")
        self.user_info_3 = QtWidgets.QGroupBox(Summary)
        self.user_info_3.setGeometry(QtCore.QRect(390, 240, 321, 441))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(50)
        self.user_info_3.setFont(font)
        self.user_info_3.setStyleSheet("font: 14pt \"MS Sans Serif\";\n"
"color: rgb(106, 0, 0);\n"
"background-color: rgb(219, 255, 240);")
        self.user_info_3.setFlat(False)
        self.user_info_3.setCheckable(False)
        self.user_info_3.setObjectName("user_info_3")
        self.weekly_trend = QtWidgets.QLabel(self.user_info_3)
        self.weekly_trend.setGeometry(QtCore.QRect(70, 10, 171, 31))
        self.weekly_trend.setStyleSheet("color: rgb(85, 170, 0);\n"
"text-decoration: underline;\n"
"font: 18pt \"Impact\";")
        self.weekly_trend.setObjectName("weekly_trend")
        self.hr = QtWidgets.QLabel(self.user_info_3)
        self.hr.setGeometry(QtCore.QRect(10, 70, 41, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.hr.setFont(font)
        self.hr.setObjectName("hr")
        self.bp = QtWidgets.QLabel(self.user_info_3)
        self.bp.setGeometry(QtCore.QRect(10, 140, 41, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.bp.setFont(font)
        self.bp.setObjectName("bp")
        self.activity = QtWidgets.QLabel(self.user_info_3)
        self.activity.setGeometry(QtCore.QRect(10, 210, 81, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.activity.setFont(font)
        self.activity.setObjectName("activity")
        self.calin = QtWidgets.QLabel(self.user_info_3)
        self.calin.setGeometry(QtCore.QRect(10, 280, 71, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.calin.setFont(font)
        self.calin.setObjectName("calin")
        self.calout = QtWidgets.QLabel(self.user_info_3)
        self.calout.setGeometry(QtCore.QRect(10, 340, 81, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.calout.setFont(font)
        self.calout.setObjectName("calout")
        self.hr_stt = QtWidgets.QLabel(self.user_info_3)
        self.hr_stt.setGeometry(QtCore.QRect(70, 70, 231, 31))
        if self.sum_hr >= 60 and self.sum_hr < 64:
            self.hr_stt.setStyleSheet("color: rgb(5, 195, 5);")  # green
            sum_heart_rate = f"{self.sum_hr}(Normal)"
        elif self.sum_hr < 60:
            self.hr_stt.setStyleSheet("color: rgb(255, 247, 5);")  # yellow
            sum_heart_rate = f"{self.sum_hr}(Slow)"
        elif self.sum_hr > 63:
            self.hr_stt.setStyleSheet("color: rgb(255, 3, 7);")  # red
            sum_heart_rate = f"{self.sum_hr}(Fast)"
        #self.hr_stt.setStyleSheet("color: rgb(11, 255, 31);")
        self.hr_stt.setText(sum_heart_rate)
        self.hr_stt.setObjectName("hr_stt")
        self.bp_stt = QtWidgets.QLabel(self.user_info_3)
        self.bp_stt.setGeometry(QtCore.QRect(80, 140, 221, 31))
        if self.sum_bp >= 120 and self.sum_bp < 124:
            self.bp_stt.setStyleSheet("color: rgb(5, 195, 5);")
            sum_bp=f"{self.sum_bp}(Normal)"
        elif self.sum_bp < 120:
            self.bp_stt.setStyleSheet("color: rgb(255, 247, 5);")
            sum_bp = f"{self.sum_bp}(Low)"
        elif self.sum_bp > 123:
            self.bp_stt.setStyleSheet("color: rgb(255, 3, 7);")
            sum_bp = f"{self.sum_bp}(High)"
        #self.bp_stt.setStyleSheet("color: rgb(11, 255, 31);")
        self.bp_stt.setText(sum_bp)
        self.bp_stt.setObjectName("bp_stt")
        self.activity_stt = QtWidgets.QLabel(self.user_info_3)
        self.activity_stt.setGeometry(QtCore.QRect(100, 210, 201, 31))
        if self.sum_ac_hr >= 1 and self.sum_ac_hr < 4:
            self.activity_stt.setStyleSheet("color: rgb(5, 195, 5);")
            ac_hr=f"{self.sum_ac_hr}hr(Normal)"
        elif self.sum_ac_hr < 1:
            self.activity_stt.setStyleSheet("color: rgb(255, 247, 5);")
            ac_hr = f"{self.sum_ac_hr}hr(Poor)"
        elif self.sum_ac_hr > 3:
            self.activity_stt.setStyleSheet("color: rgb(255, 3, 7);")
            ac_hr = f"{self.sum_ac_hr}hr(Good)"
        #self.activity_stt.setStyleSheet("color: rgb(11, 255, 31);")
        self.activity_stt.setText(ac_hr)
        self.activity_stt.setObjectName("activity_stt")
        self.calin_stt = QtWidgets.QLabel(self.user_info_3)
        self.calin_stt.setGeometry(QtCore.QRect(90, 280, 211, 31))
        if self.sum_calin >= 1000 and self.sum_calin<1500:
            self.calin_stt.setStyleSheet("color: rgb(5, 195, 5);")
            cal_in=f"{self.sum_calin}(Normal)"
        elif self.sum_calin<1000:
            self.calin_stt.setStyleSheet("color: rgb(255, 247, 5);")
            cal_in = f"{self.sum_calin}(Too Low)"
        elif self.sum_calin>1500:
            self.calin_stt.setStyleSheet("color: rgb(255, 3, 7);")
            cal_in = f"{self.sum_calin}(Too much)"
        #self.calin_stt.setStyleSheet("color: rgb(11, 255, 31);")
        self.calin_stt.setText(cal_in)
        self.calin_stt.setObjectName("calin_stt")
        self.calout_stt = QtWidgets.QLabel(self.user_info_3)
        self.calout_stt.setGeometry(QtCore.QRect(100, 340, 201, 31))
        if self.sum_calout >= 1000 and self.sum_calout < 1500:
            self.calout_stt.setStyleSheet("color: rgb(5, 195, 5);")
            sum_calout=f"{self.sum_calout}(Normal)"
        elif self.sum_calout < 1000:
            self.calout_stt.setStyleSheet("color: rgb(255, 247, 5);")
            sum_calout = f"{self.sum_calout}(Poor)"
        elif self.sum_calout > 1500:
            self.calout_stt.setStyleSheet("color: rgb(255, 3, 7);")
            sum_calout = f"{self.sum_calout}(Too High)"
        #self.calout_stt.setStyleSheet("color: rgb(11, 255, 31);")
        self.calout_stt.setText(sum_calout)
        self.calout_stt.setObjectName("calout_stt")
        self.user_info_2 = QtWidgets.QGroupBox(Summary)
        self.user_info_2.setGeometry(QtCore.QRect(730, 240, 311, 391))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(50)
        self.user_info_2.setFont(font)
        self.user_info_2.setStyleSheet("font: 14pt \"MS Sans Serif\";\n"
"color: rgb(106, 0, 0);\n"
"background-color: rgb(219, 255, 240);")
        self.user_info_2.setFlat(False)
        self.user_info_2.setCheckable(False)
        self.user_info_2.setObjectName("user_info_2")
        self.today = QtWidgets.QLabel(self.user_info_2)
        self.today.setGeometry(QtCore.QRect(110, 10, 81, 31))
        self.today.setStyleSheet("color: rgb(85, 170, 0);\n"
"text-decoration: underline;\n"
"font: 18pt \"Impact\";")
        self.today.setObjectName("today")
        self.hr_2 = QtWidgets.QLabel(self.user_info_2)
        self.hr_2.setGeometry(QtCore.QRect(10, 60, 41, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.hr_2.setFont(font)
        self.hr_2.setObjectName("hr_2")
        self.bp_2 = QtWidgets.QLabel(self.user_info_2)
        self.bp_2.setGeometry(QtCore.QRect(10, 130, 41, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.bp_2.setFont(font)
        self.bp_2.setObjectName("bp_2")
        self.activity_2 = QtWidgets.QLabel(self.user_info_2)
        self.activity_2.setGeometry(QtCore.QRect(10, 200, 81, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.activity_2.setFont(font)
        self.activity_2.setObjectName("activity_2")
        self.calin_2 = QtWidgets.QLabel(self.user_info_2)
        self.calin_2.setGeometry(QtCore.QRect(10, 270, 71, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.calin_2.setFont(font)
        self.calin_2.setObjectName("calin_2")
        self.calout_2 = QtWidgets.QLabel(self.user_info_2)
        self.calout_2.setGeometry(QtCore.QRect(10, 340, 81, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.calout_2.setFont(font)
        self.calout_2.setObjectName("calout_2")
        self.hr_info = QtWidgets.QLabel(self.user_info_2)
        self.hr_info.setGeometry(QtCore.QRect(70, 60, 131, 31))
        #self.hr_info.setStyleSheet("color: rgb(11, 255, 31);")
        if self.heart_rate >= 60 and self.heart_rate < 64: self.hr_info.setStyleSheet("color: rgb(5, 195, 5);")  # green
        elif self.heart_rate < 60: self.hr_info.setStyleSheet("color: rgb(255, 247, 5);")  # yellow
        elif self.heart_rate > 63: self.hr_info.setStyleSheet("color: rgb(255, 3, 7);")  # red
        self.hr_info.setText(self.heart_r)
        self.hr_info.setObjectName("hr_info")
        self.bp_info = QtWidgets.QLabel(self.user_info_2)
        self.bp_info.setGeometry(QtCore.QRect(90, 130, 131, 31))
        #self.bp_info.setStyleSheet("color: rgb(11, 255, 31);")
        if self.b_press >= 120 and self.b_press < 124: self.bp_info.setStyleSheet("color: rgb(5, 195, 5);")
        elif self.b_press < 120:self.bp_info.setStyleSheet("color: rgb(255, 247, 5);")
        elif self.b_press > 123: self.bp_info.setStyleSheet("color: rgb(255, 3, 7);")
        self.bp_info.setText(self.blood_p)
        self.bp_info.setObjectName("bp_info")
        self.activity_info = QtWidgets.QLabel(self.user_info_2)
        self.activity_info.setGeometry(QtCore.QRect(120, 190, 131, 31))
        #self.activity_info.setStyleSheet("color: rgb(11, 255, 31);")
        if self.ac_hour >= 1 and self.ac_hour < 4: self.activity_info.setStyleSheet("color: rgb(5, 195, 5);")
        elif self.ac_hour < 1:self.activity_info.setStyleSheet("color: rgb(255, 247, 5);")
        elif self.ac_hour > 3:self.activity_info.setStyleSheet("color: rgb(255, 3, 7);")
        self.activity_info.setText(self.ac_hr)
        self.activity_info.setObjectName("activity_info")
        self.calin_info = QtWidgets.QLabel(self.user_info_2)
        self.calin_info.setGeometry(QtCore.QRect(120, 270, 131, 31))
        #self.calin_info.setStyleSheet("color: rgb(11, 255, 31);")
        if self.calin_daily >= 1000 and self.calin_daily<1500:self.calin_info.setStyleSheet("color: rgb(5, 195, 5);")
        elif self.calin_daily<1000: self.calin_info.setStyleSheet("color: rgb(255, 247, 5);")
        elif self.calin_daily>1500: self.calin_info.setStyleSheet("color: rgb(255, 3, 7);")
        self.calin_info.setText(self.calo_in)
        self.calin_info.setObjectName("calin_info")
        self.calout_info = QtWidgets.QLabel(self.user_info_2)
        self.calout_info.setGeometry(QtCore.QRect(120, 340, 131, 31))
        #self.calout_info.setStyleSheet("color: rgb(11, 255, 31);")
        if self.calout_daily >= 1000 and self.calout_daily < 1500: self.calout_info.setStyleSheet("color: rgb(5, 195, 5);")
        elif self.calout_daily < 1000: self.calout_info.setStyleSheet("color: rgb(255, 247, 5);")
        elif self.calout_daily > 1500: self.calout_info.setStyleSheet("color: rgb(255, 3, 7);")
        self.calout_info.setText(self.calo_out)
        self.calout_info.setObjectName("calout_info")
        self.copyright = QtWidgets.QLabel(Summary)
        self.copyright.setGeometry(QtCore.QRect(0, 730, 1101, 61))
        self.copyright.setStyleSheet("font: 20pt \"Russo One\";\n"
"")
        self.copyright.setObjectName("copyright")
        self.comboBox = QtWidgets.QComboBox(Summary)
        self.comboBox.setGeometry(QtCore.QRect(810, 180, 231, 41))
        font = QtGui.QFont()
        font.setFamily("Palatino Linotype")
        font.setPointSize(14)
        self.comboBox.setFont(font)
        self.comboBox.setStyleSheet("background-color: rgb(232, 215, 255);")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        self.retranslateUi(Summary)
        QtCore.QMetaObject.connectSlotsByName(Summary)
        self.comboBox.activated[str].connect(self.combobox_selected)
    def retranslateUi(self, Summary):
        _translate = QtCore.QCoreApplication.translate
        Summary.setWindowTitle(_translate("Summary", "Health Status Summary"))
        self.f_name.setText(_translate("Summary", "First name:"))
        self.l_name.setText(_translate("Summary", "Last name:"))
        self.gender.setText(_translate("Summary", "Gender:"))
        self.wei.setText(_translate("Summary", "Weight:"))
        self.hei.setText(_translate("Summary", "Height:"))
        self.bmi.setText(_translate("Summary", "BMI:"))
        self.label.setText(_translate("Summary", "User Information"))
        self.weekly_trend.setText(_translate("Summary", "Weekly Trend"))
        self.hr.setText(_translate("Summary", "HR:"))
        self.bp.setText(_translate("Summary", "BP:"))
        self.activity.setText(_translate("Summary", "Activity:"))
        self.calin.setText(_translate("Summary", "Cal in:"))
        self.calout.setText(_translate("Summary", "Cal out:"))
        self.today.setText(_translate("Summary", "Today"))
        self.hr_2.setText(_translate("Summary", "HR:"))
        self.bp_2.setText(_translate("Summary", "BP:"))
        self.activity_2.setText(_translate("Summary", "Activity:"))
        self.calin_2.setText(_translate("Summary", "Cal in:"))
        self.calout_2.setText(_translate("Summary", "Cal out:"))
        self.copyright.setText(_translate("Summary", "                             CopyRight @ 2020 Group 9"))
        self.comboBox.setItemText(0, _translate("Summary", "Menu"))
        self.comboBox.setItemText(1, _translate("Summary", "Health Status Summary"))
        self.comboBox.setItemText(2, _translate("Summary", "Data Entry"))
        self.comboBox.setItemText(3, _translate("Summary", "Show Graph"))
        self.comboBox.setItemText(4, _translate("Summary", "Reset Password"))
        self.comboBox.setItemText(5, _translate("Summary", "Exit"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Summary = QtWidgets.QWidget()
    ui = Ui_Summary('12345')
    ui.setupUi(Summary)
    Summary.show()
    sys.exit(app.exec_())
