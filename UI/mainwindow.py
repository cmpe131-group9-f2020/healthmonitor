from PyQt5 import QtCore, QtGui, QtWidgets
from UI.signup_window import Ui_registerwindow
from UI.reset_window import Ui_Form
from UI.summary_win import Ui_Summary
from PyQt5.QtWidgets import QMessageBox
from UI.global_service import createBackendService, getBackendService

class Ui_MainWindow(object):
    def __init__(self):
        createBackendService()
        self.backendService = getBackendService()

    def resetpw_clicked(self):
        self.Form = QtWidgets.QWidget()
        self.ui = Ui_Form()
        self.ui.setupUi(self.Form)
        self.Form.show()
    def exit_clicked(self):
        QtCore.QCoreApplication.instance().quit()
    def signup_clicked(self):
        self.registerwindow = QtWidgets.QWidget()
        self.ui = Ui_registerwindow()
        self.ui.setupUi(self.registerwindow)
        self.registerwindow.show()

    def login_clicked(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="phuongngo",
            database="health_monitor"
        )
        mycursor = mydb.cursor()
        query = "SELECT id, username,passw FROM user_data WHERE username = %s"
        mycursor.execute(query, (self.email_id.text(),))
        result = mycursor.fetchall()
        if len(result)==0:
            self.email_id.clear()
            self.passw.clear()
            message = QMessageBox()
            message.setWindowTitle("FAIL !")
            message.setText("Invalid user.")
            message.exec()
        else:
            for x in result:
                id = x[0]
                userid=x[1]
                pw=x[2]
            if self.email_id.text() == userid and self.passw.text()==pw:# when user enter id and passw correctly
                self.email_id.clear()
                self.passw.clear()
                self.Summary = QtWidgets.QWidget()
                self.ui = Ui_Summary(id)
                self.ui.setupUi(self.Summary)
                self.Summary.show()
            elif self.email_id.text() == userid and self.passw.text()!=pw: # when user enter userid correctly but wrong password
                self.passw.clear()
                message = QMessageBox()
                message.setWindowTitle("FAIL !")
                message.setText("Incorrect password.")
                message.exec()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(837, 857)
        MainWindow.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, -20, 811, 251))
        self.label.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);\n"
"background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label.setText("")
        self.label.setObjectName("label")
        self.login_but = QtWidgets.QPushButton(self.centralwidget)
        self.login_but.setGeometry(QtCore.QRect(350, 400, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.login_but.setFont(font)
        self.login_but.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.login_but.setObjectName("login_but")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 720, 791, 61))
        self.label_2.setStyleSheet("font: 20pt \"Russo One\";\n"
"")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 210, 801, 61))
        self.label_3.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255));\n"
"font: 24pt \"Russo One\";\n"
"color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 226, 21, 255), stop:1 rgba(234, 8, 8, 255));")
        self.label_3.setObjectName("label_3")
        self.emailid = QtWidgets.QLabel(self.centralwidget)
        self.emailid.setGeometry(QtCore.QRect(160, 290, 161, 41))
        self.emailid.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.emailid.setObjectName("emailid")
        self.pw = QtWidgets.QLabel(self.centralwidget)
        self.pw.setGeometry(QtCore.QRect(160, 340, 161, 41))
        self.pw.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.pw.setObjectName("pw")
        self.resetpw_but = QtWidgets.QPushButton(self.centralwidget)
        self.resetpw_but.setGeometry(QtCore.QRect(180, 480, 171, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.resetpw_but.setFont(font)
        self.resetpw_but.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.resetpw_but.setObjectName("resetpw_but")
        self.signup_but = QtWidgets.QPushButton(self.centralwidget)
        self.signup_but.setGeometry(QtCore.QRect(360, 480, 171, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.signup_but.setFont(font)
        self.signup_but.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.signup_but.setObjectName("signup_but")
        self.exit_but = QtWidgets.QPushButton(self.centralwidget)
        self.exit_but.setGeometry(QtCore.QRect(540, 480, 171, 51))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.exit_but.setFont(font)
        self.exit_but.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.exit_but.setObjectName("exit_but")
        self.email_id = QtWidgets.QLineEdit(self.centralwidget)
        self.email_id.setGeometry(QtCore.QRect(330, 290, 391, 41))
        self.email_id.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.email_id.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.email_id.setObjectName("email_id")
        self.passw = QtWidgets.QLineEdit(self.centralwidget)
        self.passw.setGeometry(QtCore.QRect(330, 340, 391, 41))
        self.passw.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.passw.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passw.setObjectName("passw")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 837, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        #when buttons get clicked
        self.resetpw_but.clicked.connect(self.resetpw_clicked)
        self.signup_but.clicked.connect(self.signup_clicked)
        self.exit_but.clicked.connect(self.exit_clicked)
        self.login_but.clicked.connect(self.login_clicked)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Log In"))
        self.login_but.setText(_translate("MainWindow", "LOG IN"))
        self.label_2.setText(_translate("MainWindow", "               CopyRight @ 2020 Group 9"))
        self.label_3.setText(_translate("MainWindow", "                         Health Monitor"))
        self.emailid.setText(_translate("MainWindow", "    Email"))
        self.pw.setText(_translate("MainWindow", "Password"))
        self.resetpw_but.setText(_translate("MainWindow", "Reset PW"))
        self.signup_but.setText(_translate("MainWindow", "Register"))
        self.exit_but.setText(_translate("MainWindow", "Exit"))
import source


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
