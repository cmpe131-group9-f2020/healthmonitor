from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
# import mysql.connector


class Ui_resetpw(object):
    def __init__(self,id):
        self.id=id
    def confirm_clicked(self):
        if self.pw_lineedit.text() != self.confirmpw_lineedit.text():
            message = QMessageBox()
            message.setWindowTitle("FAIL")
            message.setText("Ooops! Password don't match.")
            message.exec()
        elif self.pw_lineedit.text() == self.confirmpw_lineedit.text():
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="phuongngo",
                database="health_monitor"
            )
            query = "UPDATE user_data SET passw=%s where id=%s"
            mycursor = mydb.cursor()
            mycursor.execute(query, (self.pw_lineedit.text(), self.id))
            mydb.commit()
            message1 = QMessageBox()
            message1.setWindowTitle("Success")
            message1.setText("Successfully reset password.")
            message1.exec()
            self.pw_lineedit.clear()
            self.confirmpw_lineedit.clear()
    def setupUi(self, resetpw):
        resetpw.setObjectName("resetpw")
        resetpw.resize(809, 557)
        resetpw.setStyleSheet("background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(62, 87, 162, 255), stop:0.781095 rgba(187, 219, 212, 255), stop:1 rgba(255, 255, 255, 255))")
        self.label = QtWidgets.QLabel(resetpw)
        self.label.setGeometry(QtCore.QRect(-100, 30, 971, 171))
        self.label.setStyleSheet("image: url(:/newPrefix/heart-rate-1375324_1280-768x576.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.newpw = QtWidgets.QLabel(resetpw)
        self.newpw.setGeometry(QtCore.QRect(10, 260, 241, 41))
        self.newpw.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.newpw.setObjectName("newpw")
        self.confirm_pw = QtWidgets.QLabel(resetpw)
        self.confirm_pw.setGeometry(QtCore.QRect(10, 320, 241, 41))
        self.confirm_pw.setStyleSheet("font: 15pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.confirm_pw.setObjectName("confirm_pw")
        self.pw_lineedit = QtWidgets.QLineEdit(resetpw)
        self.pw_lineedit.setGeometry(QtCore.QRect(260, 260, 391, 41))
        self.pw_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.pw_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.pw_lineedit.setObjectName("pw_lineedit")
        self.confirmpw_lineedit = QtWidgets.QLineEdit(resetpw)
        self.confirmpw_lineedit.setGeometry(QtCore.QRect(260, 320, 391, 41))
        self.confirmpw_lineedit.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 18pt \"Russo One\";\n"
"color: rgb(85, 5, 10)")
        self.confirmpw_lineedit.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.confirmpw_lineedit.setObjectName("confirmpw_lineedit")
        self.confirm = QtWidgets.QPushButton(resetpw)
        self.confirm.setGeometry(QtCore.QRect(240, 430, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.confirm.setFont(font)
        self.confirm.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.confirm.setObjectName("confirm")
        self.exit = QtWidgets.QPushButton(resetpw)
        self.exit.setGeometry(QtCore.QRect(470, 430, 141, 41))
        font = QtGui.QFont()
        font.setFamily("Russo One")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.exit.setFont(font)
        self.exit.setStyleSheet("font: 18pt \"Russo One\";\n"
"color: rgb(255, 247, 128);\n"
"background-color: rgb(153, 0, 28);")
        self.exit.setObjectName("exit")

        self.retranslateUi(resetpw)
        QtCore.QMetaObject.connectSlotsByName(resetpw)
        self.confirm.clicked.connect(self.confirm_clicked)
        self.exit.clicked.connect(resetpw.close)

    def retranslateUi(self, resetpw):
        _translate = QtCore.QCoreApplication.translate
        resetpw.setWindowTitle(_translate("resetpw", "Reset Password"))
        self.newpw.setText(_translate("resetpw", " New Password"))
        self.confirm_pw.setText(_translate("resetpw", "Confirm Password"))
        self.confirm.setText(_translate("resetpw", "Confirm"))
        self.exit.setText(_translate("resetpw", "exit"))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    resetpw = QtWidgets.QWidget()
    ui = Ui_resetpw()
    ui.setupUi(resetpw)
    resetpw.show()
    sys.exit(app.exec_())
