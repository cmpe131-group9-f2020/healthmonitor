import sys
import threading
import time

from .singleton import Singleton

_thread_local_store = threading.local()


class ThreadLocalStore(Singleton):
    _local_store = _thread_local_store

    def __init__(self):
        #ThreadLocalStore._local_store.datastore = {}
        pass

    def setdata(self, data):
        ThreadLocalStore._local_store.datastore = data

    def getdata(self):
        return ThreadLocalStore._local_store.datastore if hasattr(ThreadLocalStore._local_store, 'datastore') else None

    def __getattr__(self, name):
        try:
            if hasattr(ThreadLocalStore._local_store, 'datastore'):
                return ThreadLocalStore._local_store.datastore[name]
        except:
            pass
        return None

    def __setattr__(self, name, value):
        try:
            datastore = ThreadLocalStore._local_store.datastore if hasattr(ThreadLocalStore._local_store, 'datastore') else {}
            datastore = datastore if datastore else {}
            datastore[name] = value
            ThreadLocalStore._local_store.datastore = datastore
        except:
            ThreadLocalStore._local_store.datastore = {}
            ThreadLocalStore._local_store.datastore[name] = value
        # setattr(ThreadLocalStore._local_store, name, value)

    def __delattr__(self, item):
        try:
            if hasattr(ThreadLocalStore._local_store, 'datastore'):
                del ThreadLocalStore._local_store.datastore[item]
        except:
            pass


class TestThread(threading.Thread):
    def __init__(self, inc=1):
        self.inc = inc
        super(TestThread, self).__init__()
        self.local = threading.local()  # _thread_local_store

    def run(self):
        for x in range(0, 10 * self.inc, self.inc):
            time.sleep(1)
            self.local.thread_var = str(x)
            tls = ThreadLocalStore()
            print (
                threading.current_thread().name + " => thread_var={0}, {1} , {2}, {3}".format(self.local.thread_var,
                                                                                              str(tls.tid) + ", " + str(tls.rid),
                                                                                              str(ThreadLocalStore._local_store), str(tls)))
            tls.tid = str(x * 10)
            tls.rid = str(x + 5)
            print (
                threading.current_thread().name + " => thread_var={0}, {1} , {2}, {3}".format(self.local.thread_var,
                                                                                              str(tls.tid) + ", " + str(tls.rid),
                                                                                              str(ThreadLocalStore._local_store), str(self.local)))


if __name__ == "__main__":
    a = TestThread(1)
    a.start()

    b = TestThread(10)
    b.start()

    try:
        while True:
            time.sleep(0.1)
            if not (a.is_alive() and b.is_alive()):
                break
    except KeyboardInterrupt:
        sys.stdout.write("Exited")
