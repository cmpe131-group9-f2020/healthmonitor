to run the Project

make sure to create virtualenv with
```buildoutcfg
    python3 -m virtualenv .venv
    source .venv/bin/activate #MacOS
```

Install dependencies
```buildoutcfg
    pip install -r requirements.txt
```


```buildoutcfg
    export PYTHONPATH=`pwd`
    python UI/mainwindow.py 
```